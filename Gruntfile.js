module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-ember-templates');
    grunt.loadNpmTasks('grunt-bower-task');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        bower: {
            install: {
                options: {
                    targetDir: "djember/static/bower",
                    cleanTargetDir: true,
                    cleanBowerDir: true,
                    layout: 'byComponent'
                }
            }
        },

        emberTemplates: {
            compile: {
                options: {
                    //amd: true,
                    templateBasePath: /djember\/templates\/handlebars\//
                },
                files: {
                    "djember/static/js/templates/compiled.js": [
                        "djember/templates/handlebars/*.handlebars"
                    ]
                }
            }
        }
    });

    grunt.registerTask('default', ['emberTemplates']);
};
